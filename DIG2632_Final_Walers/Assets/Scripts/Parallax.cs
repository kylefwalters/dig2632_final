﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private GameObject player;
    private Vector2 pStartLocation;
    private Vector2 pCurrentLocation;
    private Vector2 pDistanceTraveled;
    public Vector3 offset;
    public float parallaxScale;
    public float trackY;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("MainCamera");
        pStartLocation = player.transform.position;
    }

    void Update()
    {
        pCurrentLocation = player.transform.position;
        pDistanceTraveled = pCurrentLocation - pStartLocation;
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y * trackY, 0) + new Vector3(pDistanceTraveled.x, pDistanceTraveled.y * trackY, 0) / parallaxScale + offset;

    }
}
