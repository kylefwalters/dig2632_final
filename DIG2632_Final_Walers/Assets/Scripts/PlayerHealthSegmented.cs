﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthSegmented : MonoBehaviour
{
    private int maxHeartAmount = 3;
    public int startHeart = 3;
    public int curHealth;
    private int maxHealth;
    private int healthPerHeart = 1;

    public bool empty = false;

    public Image[] healthImages;
    public Sprite[] healthSprites;

    void Start()
    {
        curHealth = startHeart * healthPerHeart;
        maxHealth = maxHeartAmount * healthPerHeart;
        checkHealthAmount();
    }
    
    void checkHealthAmount()
    {
        for(int i = 0; i < maxHeartAmount; i++)
        {
            if(curHealth <= i)
            {
                healthImages[i].sprite = healthSprites[0];
            }else
            {
                healthImages[i].sprite = healthSprites[1];
            }
        }
    }

    private void Update()
    {
        curHealth = player_movement.phealth;
        checkHealthAmount();
    }
}
