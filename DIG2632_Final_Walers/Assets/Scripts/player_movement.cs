﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_movement : MonoBehaviour
{
    public GameObject Player;

    public Rigidbody2D rb;

    public static int phealth = 3;
    private float currentSpeed;
    private float targetSpeed;
    private float maxSpeed;
    private float distanceGround;
    private Vector2 pVelocity;
    public Vector2 checkpoint;
    private Vector2 dashDirection = new Vector2(10, 0);

    private bool isMoving = false;
    private bool canJump = true;
    private bool facingRL = false;
    private bool isHurt = false;
    private bool dying = false;
    private bool hasPowerup;
    private bool canDash;

    public LayerMask excludeplayer;
    private Animator anim;
    private Animator maskAnim;
    private SpriteRenderer sprite;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        distanceGround = GetComponent<CapsuleCollider2D>().bounds.extents.y;
        anim = GetComponent<Animator>();
        maskAnim = GameObject.Find("Sprite Mask").GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        checkpoint = transform.position;
    }

    void FixedUpdate()
    {
        pVelocity = rb.velocity;
        //limit position
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -5.75f, 180), transform.position.y);

        //Player Death
        if (phealth <= 0 && !dying)
        {
            StartCoroutine(Death());
        }

        //Player Movement
        Player.transform.Translate(new Vector3(currentSpeed, 0, 0) * Time.deltaTime, Space.World);

        currentSpeed = Mathf.MoveTowards(currentSpeed, targetSpeed, maxSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            if (!isHurt && !dying)
            {
                isMoving = true;
                targetSpeed = -5f;
                sprite.flipX = false;
                anim.Play("PlayerWalkL");
                facingRL = true;
            }
        } else
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            if (!isHurt && !dying)
            {
                isMoving = true;
                targetSpeed = 5f;
                sprite.flipX = false;
                anim.Play("PlayerWalkR");
                facingRL = false;
            }
        } else
        {
            isMoving = false;
            if (facingRL == false && canJump == true && rb.velocity.x == 0 && dying == false)
            {
                sprite.flipX = false;
                anim.Play("PlayerIdleR");
            } else if (facingRL == true && canJump == true && rb.velocity.x == 0 && dying == false)
            {
                sprite.flipX = false;
                anim.Play("PlayerIdleL");
            }
        }

        if (targetSpeed > 0 && currentSpeed < 0 || targetSpeed < 0 && currentSpeed > 0)
        {
            maxSpeed = 20f;
        } else
        if (isMoving == false)
        {
            maxSpeed = 20f;
            targetSpeed = 0f;
        }
        else
        {
            maxSpeed = 14f;
        }

        //Player Jump
        if (Input.GetKeyDown(KeyCode.Space) && canJump == true)
        {
            rb.velocity = new Vector2(0, 15f);
            canJump = false;
        }

        //Player Dash
        if (Input.GetKeyDown(KeyCode.W) && canDash)
        {
            dashDirection = new Vector2(0, 17);
        } else if (Input.GetKeyDown(KeyCode.A) && canDash && hasPowerup)
        {
            dashDirection = new Vector2(-10, 3);
        }
        else if (Input.GetKeyDown(KeyCode.S) && canDash && hasPowerup)
        {
            dashDirection = new Vector2(0, -10);
        }
        else if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && canDash && hasPowerup || Input.GetKeyDown(KeyCode.D) && canDash && hasPowerup)
        {
            dashDirection = new Vector2(10, 3);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && dying == false && canDash && hasPowerup)
        {
            StartCoroutine(Dash());
        }
    }

    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position + transform.right * 0.5f, Vector2.down * GetComponent<BoxCollider2D>().bounds.extents.y * 1.1f);
        Gizmos.DrawRay(transform.position - transform.right * 0.5f, Vector2.down * GetComponent<BoxCollider2D>().bounds.extents.y * 1.1f);
    }*/

    public void OnCollisionEnter2D(Collision2D collision)
    {
        //Player canJump Ray Detection
        if (Physics2D.Raycast(transform.position, Vector2.down, distanceGround + 0.1f, excludeplayer) || Physics2D.Raycast(transform.position + transform.right * 0.1f, Vector2.down, distanceGround + 0.1f, excludeplayer) || Physics2D.Raycast(transform.position - transform.right * 0.5f, Vector2.down, distanceGround + 0.1f, excludeplayer))
        {
            canJump = true;
            canDash = true;
        }
        else
        {
            canJump = false;
        }

        if(collision.gameObject.tag == "Checkpoint")
        {
            checkpoint = collision.gameObject.transform.position;
            collision.gameObject.SetActive(false);
        }

        if (collision.gameObject.tag == "Enemy")
        {
            if(Player.transform.position.y * 1.05f >= collision.gameObject.transform.position.y + collision.gameObject.GetComponent<BoxCollider2D>().bounds.extents.y + distanceGround)
            {
                //Debug.Log("Bounce");
                canJump = true;
                StartCoroutine("Bounce");
            }
            else if (Player.transform.position.y * 1.05f <= collision.gameObject.transform.position.y + collision.gameObject.GetComponent<BoxCollider2D>().bounds.extents.y + distanceGround && isHurt == false )
            {
                StartCoroutine(Damage());
            }
        }

        if (collision.gameObject.tag == "Death")
        {
            rb.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
            phealth = 0;
            StartCoroutine(Death());
        }

        if (collision.gameObject.name == "DashPowerup")
        {
            canDash = true;
            hasPowerup = true;
            collision.gameObject.SetActive(false);
        }

        if(collision.gameObject.name == "Health")
        {
            phealth += 1;
            collision.gameObject.SetActive(false);
        }
    }

    


    IEnumerator Bounce()
    {
        yield return new WaitForSecondsRealtime(.05f);
        if (canJump == true)
        {
            canJump = false;
            rb.velocity = new Vector2(rb.velocity.x, 10f);
        }else
        {
            while(Time.timeScale > 0.45f)
            {
                Time.timeScale -= 0.03f;
                yield return new WaitForSeconds(.01f);
            }
            Time.timeScale = 0.45f;
            //Application.targetFrameRate = 30;
            yield return new WaitForSecondsRealtime(0.55f);
            //Application.targetFrameRate = -1;
            while (Time.timeScale < 1f)
            {
                if(Time.timeScale != 0)
                {
                    Time.timeScale += 0.01f;
                    yield return new WaitForSeconds(.01f);
                }
            }
            Time.timeScale = 1;
        }
    }

    IEnumerator Damage()
    {
        if(phealth > 0 && !dying)
        {
            isHurt = true;
            phealth -= 1;
            anim.Play("PlayerDamage");
            //Debug.Log("Damage");
            targetSpeed = 0;
            if (!facingRL)
            {
                rb.velocity = new Vector2(-5f, 10f);
            }else
            {
                rb.velocity = new Vector2(5f, 10f);
                sprite.flipX = true;
            }
            yield return new WaitForSeconds(0.75f);
            isHurt = false;
            canJump = true;
        }
    }

    IEnumerator Death()
    {
        dying = true;
        anim.Play("PlayerDying");
        maskAnim.Play("DeathSpriteMask");
        yield return new WaitForSeconds(5.5f);
        transform.position = checkpoint;
        phealth = 3;
        maskAnim.Play("MaskDefaultState");
        dying = false;
        rb.constraints = ~RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
    }

    IEnumerator Dash()
    {
        rb.velocity = dashDirection;
        canDash = false;
        yield return null;
    }
}
