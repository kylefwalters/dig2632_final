﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public int eHealth;

    private float pDetection;
    private float eSize = 0.6549997f;
    //private float pDistance;

    private bool pIsNear;

    private Vector3 eVectorBounds;

    private GameObject player;

    private Rigidbody2D rb;

    private Animator anim;

    private Renderer m_Renderer;

    void Start()
    {
        pDetection = GetComponent<BoxCollider2D>().bounds.extents.y + 0.05f;
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
        eSize = GetComponent<BoxCollider2D>().bounds.extents.x;
        eVectorBounds = new Vector3(eSize * 0.85f, 0, 0);
        m_Renderer = gameObject.GetComponent<Renderer>();
        anim = GetComponent<Animator>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawRay(transform.position, Vector2.up);
        Gizmos.DrawRay(transform.position - eVectorBounds, Vector2.up);
        Gizmos.DrawRay(transform.position + eVectorBounds, Vector2.up);
    }

    public void FixedUpdate()
    {
        if(pIsNear == true && eHealth > 0)
        {
            transform.Translate(Vector2.left * Time.deltaTime);
        }
    }

    public void Update()
    {
        Physics2D.queriesStartInColliders = false;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up * transform.localScale.y, pDetection);
        RaycastHit2D hitL = Physics2D.Raycast(transform.position - eVectorBounds, Vector2.up * transform.localScale.y, pDetection);
        RaycastHit2D hitR = Physics2D.Raycast(transform.position + eVectorBounds, Vector2.up * transform.localScale.y, pDetection);
        if (hit.collider != null && hit.collider.gameObject.tag == "Player" || hitL.collider != null && hitL.collider.gameObject.tag == "Player" || hitR.collider != null && hitR.collider.gameObject.tag == "Player")
        {
            eHealth -= 1;
            if(eHealth > 0)
            {
                rb.velocity = new Vector2(-5f, rb.velocity.y);
            }
        }

        if (eHealth <= 0)
        {
            StartCoroutine(Death());
        }

        //checks if player is near
        //pDistance = Vector2.Distance(transform.position, player.transform.position);
        /*if (pDistance <= 11)
        {
            pIsNear = true;
        }else
        {
            pIsNear = false;
        }*/
        if(m_Renderer.isVisible == true)
        {
            pIsNear = true;
        }else
        {
            pIsNear = false;
        }
    }

    public void CollisionDetectedChild(Collision2D collision)
    {
        //Collider2D myCollider = collision.contacts[0].collider;
        eHealth -= 1;
        if (eHealth > 0)
        {
            rb.velocity = new Vector2(12f, rb.velocity.y);
        }
    }
    IEnumerator Death()
    {
        anim.Play("SlimeDying");
        yield return new WaitForSecondsRealtime(0.5f);
        Destroy(gameObject);
    }
}