﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public Vector3 offset;
    public Vector3 velocity = Vector3.zero;

    void FixedUpdate()
    {
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, 3, 170), Mathf.Clamp(transform.position.y, -7, 20));

        if(target != null)
        {
            transform.position = Vector3.SmoothDamp(transform.position, target.position + offset, ref velocity, 0.6f);
        }
    }
}
