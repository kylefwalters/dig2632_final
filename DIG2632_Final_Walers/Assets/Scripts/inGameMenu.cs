﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inGameMenu : MonoBehaviour
{
    private float curAlpha;
    private CanvasGroup image;
    public GameObject MenuButtons;
    private CanvasGroup buttonGroup;
    private bool menuActive;
    private float targetAlpha;

    private float buttonAlpha;
    private int buttonAlphaTarget = 0;

    public float fadeRate;

    void Start()
    {
        image = GetComponent<CanvasGroup>();
        buttonGroup = MenuButtons.GetComponent<CanvasGroup>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
    }

    void Update()
    {
        curAlpha = Mathf.Lerp(curAlpha, targetAlpha, fadeRate * Time.unscaledDeltaTime);
        buttonAlpha = Mathf.Lerp(buttonAlpha, buttonAlphaTarget, fadeRate * Time.unscaledDeltaTime);
        if(buttonAlpha == 0)
        {
            //MenuButtons.SetActive(false);
        }
        if(Input.GetKeyDown(KeyCode.Escape) && menuActive == false && player_movement.phealth != 0)
        {
            StartCoroutine(FadeInOut());
            
        }else if(Input.GetKeyDown(KeyCode.Escape) && menuActive == true && player_movement.phealth != 0)
        {
            StartCoroutine(FadeInOut());
        }
        image.alpha = curAlpha;
        buttonGroup.alpha = buttonAlpha;
    }

    public void Resume()
    {
        if(menuActive == true)
        {
            StartCoroutine(FadeInOut());
        }
    }

    public void Exit()
    {
        Debug.Log("Quitting");
        Application.Quit();
    }

    IEnumerator FadeInOut()
    {
        if(menuActive == false)
        {
            targetAlpha = 0.5f;
            menuActive = true;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;
        }
        else if (menuActive == true)
        {
            targetAlpha = 0;
            buttonAlphaTarget = 0;
            menuActive = false;
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = true;
        }
        yield return new WaitForSecondsRealtime(0.3f);
        if (menuActive == true)
        {
            //MenuButtons.SetActive(true);
            buttonAlphaTarget = 1;
        }else
        {
            buttonAlphaTarget = 0;
        }
    }
}
